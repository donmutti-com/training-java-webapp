package ru.digisign.stuff.javawebapp.dao;

import ru.digisign.stuff.javawebapp.model.Article;

import java.util.List;

public interface ArticleDAO {

    Integer getLastId();

    Integer getNextId();

    public void insert(Article article);

    public Object findById(int id);

    List<Article> list();

    void delete(int id);
}
