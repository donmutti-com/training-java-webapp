package ru.digisign.stuff.javawebapp.dao.impl.hibernate;

import ru.digisign.stuff.javawebapp.dao.ArticleDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.digisign.stuff.javawebapp.model.Article;

import java.util.List;

@Repository
public class ArticleDAOHibernateImpl implements ArticleDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Integer getLastId() {
        return null;
    }

    @Override
    public Integer getNextId() {
        return null;
    }

    public void insert(Article article) {
        Session session = sessionFactory.getCurrentSession();
        session.save(article);
    }

    @Override
    public Article findById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (Article) session.get(Article.class, id);
    }

    @Override
    public List<Article> list() {
        return null;
    }

    @Override
    public void delete(int id) {
        Article article = findById(id);
        if (article != null) {
            Session session = sessionFactory.getCurrentSession();
            session.delete(article);
        }
    }
}
