package ru.digisign.stuff.javawebapp.service;

import ru.digisign.stuff.javawebapp.model.Article;

import java.util.List;

public interface ArticleService {

    List<Article> list();

    void insert(Article article);

    void delete(int id);
}
