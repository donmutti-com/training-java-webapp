package ru.digisign.stuff.javawebapp.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.digisign.stuff.javawebapp.dao.ArticleDAO;
import ru.digisign.stuff.javawebapp.model.Article;
import ru.digisign.stuff.javawebapp.service.ArticleService;

import java.util.List;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {

    private ArticleDAO dao;

    public ArticleDAO getDao() {
        return dao;
    }

    public void setDao(ArticleDAO dao) {
        this.dao = dao;
    }

    @Override
    public List<Article> list() {
        return getDao().list();
    }

    @Override
    public void insert(Article article) {
        getDao().insert(article);
    }

    @Override
    public void delete(int id) {
        getDao().delete(id);
    }
}
