package ru.digisign.stuff.javawebapp.dao.impl.jdbc;

import ru.digisign.stuff.javawebapp.dao.ArticleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.digisign.stuff.javawebapp.model.Article;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class ArticleDAOImpl implements ArticleDAO {

    @Autowired
    private DataSource dataSource;

    @Override
    public Integer getLastId() {
        String sql = "SELECT last_value from article_id_seq";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            stmt.close();
            if (rs.isBeforeFirst()) {
                return rs.getInt(1);
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }

    @Override
    public Integer getNextId() {
        String sql = "SELECT nextval('article_id_seq')";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            stmt.close();
            if (rs.isBeforeFirst()) {
                return rs.getInt(1);
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }

    @Override
    public void insert(Article article) {
        String sql = "INSERT INTO article (id, create_date, title, content) VALUES (?, ?, ?, ?)";
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, article.getId());
            stmt.setTimestamp(2, new Timestamp(article.getCreateDate().getTime()));
            stmt.setString(3, article.getTitle());
            stmt.setString(4, article.getContent());
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }

    @Override
    public Article findById(int id) {
        String sql = "SELECT id, create_date, title, content FROM article WHERE id = ?";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            Article article = null;
            if (rs.next()) {
                article = new Article();
                article.setId(rs.getInt(1));
                article.setCreateDate(new Date(rs.getTimestamp(2).getTime()));
                article.setTitle(rs.getString(3));
                article.setContent(rs.getString(4));
            }
            rs.close();
            stmt.close();
            return article;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }

    @Override
    public List<Article> list() {
        String sql = "SELECT id, create_date, title, content FROM article ORDER BY create_date";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            List<Article> list = new ArrayList<>();
            if (rs.isBeforeFirst()) {
                while (rs.next()) {
                    Article article = new Article();
                    article.setId(rs.getInt(1));
                    article.setCreateDate(new Date(rs.getTimestamp(2).getTime()));
                    article.setTitle(rs.getString(3));
                    article.setContent(rs.getString(4));
                    list.add(article);
                }
            }
            rs.close();
            stmt.close();
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE from article WHERE id = ?";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try { conn.close(); } catch (SQLException e) {}
            }
        }
    }
}
