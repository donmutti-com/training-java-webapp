package ru.digisign.stuff.javawebapp;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("HelloServlet")
public class HelloServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();


        writer.println("<html>");
        writer.println("<head>");
        writer.println("    <title>Java Web App</title>");
        writer.println("    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/styles.css\"/>");
        writer.println("</head>");
        writer.println("");
        writer.println("<body>");
        writer.println("");
        writer.println("<h1>Hello Page</h1>");
        writer.println("");
        writer.println("<p>");
        writer.println("    This is the output text generated directly from HttpServlet code.");
        writer.println("</p>");
        writer.println("");
        writer.println("</body>");
        writer.println("</html>");
    }
}
