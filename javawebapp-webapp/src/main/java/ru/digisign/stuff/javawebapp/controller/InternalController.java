package ru.digisign.stuff.javawebapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class InternalController {

  @RequestMapping(value = "/internal", method = RequestMethod.GET)
  public ModelAndView internalPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
    ModelAndView model = new ModelAndView("internal");
    return model;
  }

}
