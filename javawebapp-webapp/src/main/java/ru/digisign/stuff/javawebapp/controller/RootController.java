package ru.digisign.stuff.javawebapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class RootController {

    private static Map<String, Integer> mapClients = new HashMap<String, Integer>();

    @RequestMapping(value = {"", "/", "/index"}, method = RequestMethod.GET)
    public String rootPage(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String clientName = request.getRemoteAddr();
        Integer clientCount = mapClients.get(clientName);
        clientCount = clientCount == null ? 1 : clientCount + 1;
        mapClients.put(clientName, clientCount);

        modelMap.addAttribute("mapClients", mapClients);

        return "index";
    }

}
