package ru.digisign.stuff.javawebapp.controller.jspsite;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.digisign.stuff.javawebapp.model.Article;
import ru.digisign.stuff.javawebapp.service.ArticleService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 *
 */
@Controller
@RequestMapping(value = JspSiteController.SITE_ROOT)
public class JspSiteController {

    @Autowired
    ArticleService articleService;

    @Autowired
    ArticleService articleServiceHibernate;

    // Site Context Root
    public static final String SITE_ROOT = "/jsp-site";

    // URLs
    private static final String URL_HOME = "/index";
    private static final String URL_LOGIN = "/login/form";
    private static final String URL_ADMIN = "/admin";
    private static final String URL_ARTICLES = "/articles";
    private static final String URL_ARTICLE_JDBC_ADD = "/article/jdbc/add";
    private static final String URL_ARTICLE_JDBC_DELETE_LAST = "/article/jdbc/deleteLast";
    private static final String URL_ARTICLE_HIBERNATE_ADD = "/article/hibernate/add";
    private static final String URL_ARTICLE_HIBERNATE_DELETE_LAST = "/article/hibernate/deleteLast";

    // URL Parameters
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_LOGOUT = "logout";

    // Views
    private static final String PAGE_HOME = "/index";
    private static final String PAGE_LOGIN = "/login/form";
    private static final String PAGE_ADMIN = "/admin";
    private static final String PAGE_ARTICLES = "/articles";

    // View Attributes
    private static final String ATTR_ERROR = "error";
    private static final String ATTR_MESSAGE = "msg";
    private static final String ATTR_ARTICLES = "articles";


    /**
     * Creates a <code>ModelAndView</code> by the given view name
     *
     * @param viewName the name of requested page
     * @return resulting <code>ModelAndView</code>
     */
    private ModelAndView getModelAndView(String viewName) {
        return new ModelAndView(SITE_ROOT + viewName);
    }

    /**
     * @return Home page
     */
    @RequestMapping(value = {"", "/", URL_HOME}, method = RequestMethod.GET)
    public ModelAndView homePage() {
        ModelAndView model = getModelAndView(PAGE_HOME);
        return model;
    }

    /**
     * @return Login page
     */
    @RequestMapping(value = URL_LOGIN, method = RequestMethod.GET)
    public ModelAndView loginPage(
        @RequestParam(value = PARAM_ERROR, required = false) String error,
        @RequestParam(value = PARAM_LOGOUT, required = false) String logout
    ) {
        ModelAndView model = getModelAndView(PAGE_LOGIN);
        if (error != null) {
            model.addObject(ATTR_ERROR, "Invalid username or password!");
        }
        if (logout != null) {
            model.addObject(ATTR_MESSAGE, "You've been logged out successfully.");
        }
        return model;
    }

    /**
     * @return Admin page
     */
    @RequestMapping(value = URL_ADMIN, method = RequestMethod.GET)
    public ModelAndView adminPage() {
        return getModelAndView(PAGE_ADMIN);
    }

    /**
     * @return Articles page
     */
    @RequestMapping(value = URL_ARTICLES, method = RequestMethod.GET)
    public ModelAndView articlesPage() {
        ModelAndView model = getModelAndView(PAGE_ARTICLES);
        List<Article> list = articleService.list();
        model.addObject(ATTR_ARTICLES, list);
        return model;
    }

    /**
     * @return Add Article URL
     */
    @RequestMapping(value = URL_ARTICLE_JDBC_ADD, method = RequestMethod.GET)
    public void articleAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Insert a new article
        Article article = new Article();
        int id = 0;
        Random rand = new Random();
        while (id <= 1) {
            id = rand.nextInt(Integer.MAX_VALUE);
        }
        article.setId(id);
        article.setTitle("JDBC Article");
        article.setCreateDate(new Date());
        articleService.insert(article);

        // Forward to the Articles page
        RequestDispatcher rd = request.getRequestDispatcher(SITE_ROOT + URL_ARTICLES);
        rd.forward(request, response);
    }

    /**
     * @return JDBC: Delete Last URL
     */
    @RequestMapping(value = URL_ARTICLE_JDBC_DELETE_LAST, method = RequestMethod.GET)
    public void articleDeleteLast(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Delete the last article
        List<Article> list = articleService.list();
        if (!list.isEmpty()) {
            articleService.delete(list.get(list.size() - 1).getId());
        }

        // Forward to the Articles page
        RequestDispatcher rd = request.getRequestDispatcher(SITE_ROOT + URL_ARTICLES);
        rd.forward(request, response);
    }

    /**
     * @return Hibernate: Add URL
     */
    @RequestMapping(value = URL_ARTICLE_HIBERNATE_ADD, method = RequestMethod.GET)
    public void articleAddHibernate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Insert a new article
        Article article = new Article();
        article.setTitle("Hibernate Article");
        article.setCreateDate(new Date());
        articleServiceHibernate.insert(article);

        // Forward to the Articles page
        RequestDispatcher rd = request.getRequestDispatcher(SITE_ROOT + URL_ARTICLES);
        rd.forward(request, response);
    }

    /**
     * @return Hibernate: Delete Last URL
     */
    @RequestMapping(value = URL_ARTICLE_HIBERNATE_DELETE_LAST, method = RequestMethod.GET)
    public void articleDeleteLastHibernate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Delete the last article
        List<Article> list = articleService.list();
        if (!list.isEmpty()) {
            articleServiceHibernate.delete(list.get(list.size() - 1).getId());
        }

        // Forward to the Articles page
        RequestDispatcher rd = request.getRequestDispatcher(SITE_ROOT + URL_ARTICLES);
        rd.forward(request, response);
    }

}

