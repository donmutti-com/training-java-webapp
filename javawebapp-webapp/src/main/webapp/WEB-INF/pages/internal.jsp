<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Java Web App</title>
    <jsp:include page="include/head.jsp"/>
</head>

<body>
<jsp:include page="include/header.jsp"/>

<div class="content">
    <h1>Internal JSP</h1>

    <p>
        This is an internal JSP, served by Spring MVC controller.
    </p>
</div>

<jsp:include page="include/footer.jsp"/>
</body>
</html>
