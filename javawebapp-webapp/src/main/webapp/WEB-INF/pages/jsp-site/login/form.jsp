<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page session="false" %>

<html>
<head>
    <title>Java Web App - JSP Site</title>
    <jsp:include page="../../include/head.jsp"/>
</head>

<body>

<jsp:include page="../include/header.jsp"/>

<c:url value="/" var="contextRoot"/>

<div class="content">
    <table width="100%">
        <tr>
            <td>
                <a href="${contextRoot}jsp-site">Home</a> | <a href="${contextRoot}jsp-site/articles">Articles</a> | <a href="${contextRoot}jsp-site/admin">Admin</a>
            </td>
            <td align="right">
                <c:choose>
                    <c:when test="${pageContext.request.userPrincipal.name != null}">
                        <b>${pageContext.request.userPrincipal.name}</b> (<a href="javascript:document.logoutForm.submit();">logout</a>)
                        <form name="logoutForm" action="${contextRoot}jsp-site/logout" method="post" style="display:none;"><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/></form>
                    </c:when>
                    <c:otherwise>
                        <a href="${contextRoot}jsp-site/login/form">Login</a>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>

    <h3>Login with Username and Password</h3>

    <form name="loginForm" action="${contextRoot}jsp-site/login" method="post">

        <table>
            <tr>
                <td colspan="2">
                    <c:if test="${not empty error}">
                        <div class="error">${error}</div>
                    </c:if>
                    <c:if test="${not empty msg}">
                        <div class="msg">${msg}</div>
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>User:</td>
                <td><input type='text' name='username' value=''></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input type='password' name='password'/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input name="submit" type="submit" value="Login"/>
                    <a href="javascript:history.back()">Cancel</a>
                </td>
            </tr>
        </table>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</div>

<jsp:include page="../include/footer.jsp"/>

</body>
</html>