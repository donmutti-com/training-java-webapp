<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Java Web App - JSP Site</title>
    <jsp:include page="../include/head.jsp"/>
</head>

<body>

<jsp:include page="include/header.jsp"/>

<c:url value="/" var="contextRoot"/>

<div class="content">

    <table width="100%">
        <tr>
            <td>
                <a href="${contextRoot}jsp-site">Home</a> | Articles | <a href="${contextRoot}jsp-site/admin">Admin</a>
            </td>
            <td align="right">
                <c:choose>
                    <c:when test="${pageContext.request.userPrincipal.name != null}">
                        <b>${pageContext.request.userPrincipal.name}</b> (<a href="javascript:document.logoutForm.submit();">logout</a>)
                        <form name="logoutForm" action="${contextRoot}jsp-site/logout" method="post" style="display:none;"><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/></form>
                    </c:when>
                    <c:otherwise>
                        <a href="${contextRoot}jsp-site/login/form">Login</a>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>

    <h3>Articles</h3>

    <p>
        Here is the full list of articles.
    </p>

    <p>

    <table>
        <tr>
            <td>
                <b>JDBC</b>
            </td>
            <td>
                <div class="toolbar">
                    <ul>
                        <li><a href="${contextRoot}jsp-site/article/jdbc/add">Add</a></li>
                        <li><a href="${contextRoot}jsp-site/article/jdbc/deleteLast">Delete Last</a></li>
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <b>Hibernate</b>
            </td>
            <td>
                <div class="toolbar">
                    <ul>
                        <li><a href="${contextRoot}jsp-site/article/hibernate/add">Add</a></li>
                        <li><a href="${contextRoot}jsp-site/article/hibernate/deleteLast">Delete Last</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    </table>

    </p>
    <c:forEach var="article" items="${articles}">
        <div class="article">
            <span class="date"><fmt:formatDate value="${article.getCreateDate()}" type="both"/></span> <span class="title">${article.getTitle()} #${article.getId()}</span>
        </div>
    </c:forEach>
</div>

<jsp:include page="include/footer.jsp"/>
</body>
</html>

