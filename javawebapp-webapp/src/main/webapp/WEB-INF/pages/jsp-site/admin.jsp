<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Java Web App - JSP Site</title>
    <jsp:include page="../include/head.jsp"/>
</head>

<body>

<jsp:include page="include/header.jsp"/>

<c:url value="/" var="contextRoot"/>

<div class="content">
    <table width="100%">
        <tr>
            <td>
                <a href="${contextRoot}jsp-site">Home</a> | <a href="${contextRoot}jsp-site/articles">Articles</a> | Admin
            </td>
            <td align="right">
                <c:choose>
                    <c:when test="${pageContext.request.userPrincipal.name != null}">
                        <b>${pageContext.request.userPrincipal.name}</b> (<a href="javascript:document.logoutForm.submit();">logout</a>)
                        <form name="logoutForm" action="${contextRoot}jsp-site/logout" method="post" style="display:none;"><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/></form>
                    </c:when>
                    <c:otherwise>
                        <a href="${contextRoot}jsp-site/login/form">Login</a>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>

    <h3>Admin</h3>

    <p>
        This is the administration page.
    </p>
</div>

<jsp:include page="include/footer.jsp"/>
</body>
</html>

