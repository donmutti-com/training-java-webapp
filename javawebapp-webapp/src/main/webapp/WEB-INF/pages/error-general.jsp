<%@ page contentType="text/html; charset=UTF-8" import="java.io.StringWriter,java.io.PrintWriter" session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Digisign</title>
    <jsp:include page="include/head.jsp"/>
</head>
<body>
<jsp:include page="include/header.jsp"/>

<c:set var="statusCode" value="${pageContext.errorData.statusCode}"/>
<table>
    <tr>
        <td width="130">
            <p class="error-page-http-status-code">
                ${statusCode}
            </p>
        </td>
        <td width="20"/>
        <td>
            <h1>
                <c:choose>
                    <c:when test="${statusCode == 400}">Bad Request</c:when>
                    <c:when test="${statusCode == 401}">Unauthorized</c:when>
                    <c:when test="${statusCode == 402}">Payment Required</c:when>
                    <c:when test="${statusCode == 403}">Forbidden</c:when>
                    <c:when test="${statusCode == 404}">Not Found</c:when>
                    <c:when test="${statusCode == 405}">Method ${pageContext.request.method} not allowed</c:when>
                    <c:when test="${statusCode == 500}">Internal Server Error</c:when>
                    <c:otherwise>${statusCode}</c:otherwise>
                </c:choose>
            </h1>

            <p>Request URI: ${pageContext.request.scheme}://${header.host}${pageContext.errorData.requestURI}</p>
            <a href="javascript:history.back()">Go Back</a>

        </td>
    </tr>
    <c:if test="${statusCode == 500}">
        <tr>
            <td colspan="3">
                <p class="error-page-stacktrace">
                    <%
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        Throwable t = pageContext.getErrorData().getThrowable();
                        t.printStackTrace(pw);
                    %>
                    <%=sw.toString()%>
                </p>
            </td>
        </tr>
    </c:if>
</table>

</body>
</html>
