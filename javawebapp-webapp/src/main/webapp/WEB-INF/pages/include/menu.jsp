<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url var="contextRoot" value="/"/>

<%-- Build a list of menu items --%>
<jsp:useBean id="map" class="java.util.HashMap"/>
${map.put("", "home")}
${map.put("hello", "home")}
${map.put("external", "home")}
${map.put("internal", "home")}
${map.put("jsp-site", "jsp-site")}
${map.put("angular-site", "angular-site")}
${map.put("jqueryui-site", "jqueryui-site")}

<%-- Get current page --%>
<c:set var="currentPage" value="${requestScope['javax.servlet.forward.request_uri']}" />
<c:set var="currentPage" value="${fn:substringAfter(currentPage, contextRoot)}" />
<c:set var="currentPage" value="${fn:split(currentPage, '/')[0]}" />
<c:set var="currentPage" value="${map.get(currentPage)}" />

<%-- Render menu --%>
<ul id="menu-main">
    <li class="${currentPage == 'home' ? 'active' : ''}"><a href="${contextRoot}">Home</a></li>
    <li class="${currentPage == 'jsp-site' ? 'active' : ''}"><a href="jsp-site">JSP site</a></li>
    <li class="${currentPage == 'angular-site' ? 'active' : ''}"><a href="angular-site">AngularJS site</a></li>
    <li class="${currentPage == 'jqueryui-site' ? 'active' : ''}"><a href="jqueryui-site">JQuery UI site</a></li>
    <ul id="logo">
        <li><a href="${applicationScope.url}/stuff/javawebapp">JavaWebApp</a></li>
        <li><a href="${applicationScope.url}/stuff">Stuff</a></li>
    </ul>
</ul>
