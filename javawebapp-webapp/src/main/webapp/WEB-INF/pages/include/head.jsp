<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:url var="contextRoot" value="/"/>

<link rel="stylesheet" type="text/css" href="${contextRoot}css/styles.css"/>
<link rel="icon" type="image/x-icon" href="${contextRoot}img/favicon.ico"/>

