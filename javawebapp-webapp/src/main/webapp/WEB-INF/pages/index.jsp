<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Java Web App</title>
    <jsp:include page="include/head.jsp"/>
</head>

<body>
<jsp:include page="include/header.jsp"/>

<div class="content">
    <h1>Main menu</h1>
    <ol>
        <li><a href="hello">hello</a>: an HttpServlet-generated page.</li>
        <li><a href="external.jsp">external.jsp</a>: an external JSP page.</li>
        <li><a href="internal">internal</a>: an internal JSP page.</li>
        <li><a href="jsp-site">jsp-site</a>: a mini JSP site under its own context sub-root.</li>
    </ol>

    <h3>Our last clients:</h3>

    <div class="client">
        <ul>
            <c:forEach var="pair" items="${mapClients}">
                <li>
                    <span class="name">${pair.key}</span> <span class="count">${pair.value}</span>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>

<jsp:include page="include/footer.jsp"/>
</body>
</html>
