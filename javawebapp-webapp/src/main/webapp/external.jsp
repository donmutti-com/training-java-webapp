<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Java Web App</title>
    <jsp:include page="WEB-INF/pages/include/head.jsp"/>
</head>

<body>
<jsp:include page="WEB-INF/pages/include/header.jsp"/>

<div class="content">
    <h1>External JSP</h1>

    <p>
        This is an external JSP, which is accessible by direct link and served by servlet container.
    </p>
</div>

<jsp:include page="WEB-INF/pages/include/footer.jsp"/>
</body>
</html>